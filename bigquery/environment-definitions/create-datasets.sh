#!/bin/bash
# source ./demo1/bigquery/environment-definitions/env-dev

bq --location=US mk \
--dataset \
--description '${DATASET_AUDIT_DESCRIPTION}' \
${PROJECT_ID}:${DATASET_AUDIT}

